/*
	TODO:
	- Keep list of images being loaded.
	  If they're out of range, kill the threads
	- Make loading work properly!
	   It should be loading one image, then loading the next so that IT DOESN'T NEED TO BE RELOADED WHEN IT IS REQUESTED




*/


import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.io.File;
import java.io.FileFilter;
import java.util.Arrays;


public class ImageViewer extends JFrame implements KeyListener {
	protected ImageFile directory[];
	public static String imageType[] = {
		"gif", "jpg", "jpeg", "png"
	};
	public final int DEFAULT_WIDTH=800, DEFAULT_HEIGHT=600;

	ImageIcon icon=null;
	ImagePanel imagePanel=null;
	int currentFile=0;
	ImageViewer me;
	MovieButton movieButton;
	JLabel statusBar;
	JButton nextButton, prevButton, scaleButton, reloadButton;
	boolean scaleImage=true;	// If true, automatically scale image to fit the display window.


	public ImageViewer() { this(null); }
	public ImageViewer(String filename) {
		// So that we can reference this from within a nested class
		me = this;

		
		// TODO: open the file as the first to view
		currentFile = 0;
		//System.out.println("Arg: " + filename);

		// Get list of all image files
		grabDirectoryTree(filename);
		if(directory.length == 0) {
			System.err.println("No image files found.");
			return;
		}
		
		CleanupThread ct = new CleanupThread(this,directory);
		ct.start();

		// Make panel for buttons
		statusBar = new JLabel("Ready.");
		prevButton = new JButton("<-- Previous");
		prevButton.addActionListener(
			new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					prevImage();
				}
			}
		);
		
		scaleButton = new JButton("Original Size");
		scaleButton.addActionListener(
			new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if(scaleImage) {
						scaleImage = false;
						scaleButton.setText("Fit to Window");
						loadImage();
					}
					else {
						scaleImage = true;
						scaleButton.setText("Original Size");
						loadImage();
					}
				}
			}
		);
			
		movieButton = new MovieButton(this);
		nextButton = new JButton("Next -->");
		nextButton.addActionListener(
			new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					nextImage();
				}
			}
		);
		
		reloadButton = new JButton("Reload");
		reloadButton.addActionListener(
			new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					reloadImage();
				}
			}
		);
			
			
		
		addComponentListener(new ComponentAdapter() {
			public void componentResized(ComponentEvent e) {
				// wait for resizing to stop before reloading, put in a different event (!)
				//if(scaleImage) loadImage();
			}
		});
		
		
		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new GridLayout(1,3));
		buttonPane.add(prevButton);
		buttonPane.add(scaleButton);
		buttonPane.add(movieButton);
		buttonPane.add(reloadButton);
		buttonPane.add(nextButton);

		
		Container c = getContentPane();
		c.setLayout(new BorderLayout());
		c.add(buttonPane, BorderLayout.NORTH);
		c.add(statusBar, BorderLayout.SOUTH);
		addKeyListener(this);
		//addWindowListener(this);
		//setJMenuBar(new ImageViewerMenuBar(this));
			
		// TODO: have it resize to fit the entire image.
		setSize(DEFAULT_WIDTH,DEFAULT_HEIGHT);


		// The image
		loadImage();
		show();
	}
	
	
	public void reloadImage() {
		LoadImageThread t = new LoadImageThread(this,currentFile,0,true);
		t.start();
		statusBar.setText("Reloading...");
	}

	public void loadImage() {
		LoadImageThread t = new LoadImageThread(this,currentFile,1,true);
		t.start();
		statusBar.setText("Loading...");
	}

	public void setImage(int number) {
		currentFile = number;
		if(currentFile >= directory.length) currentFile = 0;
		loadImage();
	}
	
	
	public void nextImage() {
		if(++currentFile >= directory.length) currentFile = 0;
		loadImage();
	}
	
	public void prevImage() {
		if(--currentFile < 0) currentFile = directory.length-1;
		loadImage();
	}
	
	public void keyTyped(KeyEvent e) {
		int code = e.getKeyCode();
		switch(code) {
			case KeyEvent.VK_LEFT:
				prevImage();
				break;
			case KeyEvent.VK_RIGHT:
				nextImage();
				break;
			case KeyEvent.VK_F5:
				reloadImage();
				break;
		}
	}
	public void keyPressed(KeyEvent e) {}
	public void keyReleased(KeyEvent e) {}
	
		
	public static void main(String args[]) {
		String arg;
		if(args.length == 0) arg = null;
		else arg = args[0];

		ImageViewer theApp = new ImageViewer(arg);
		theApp.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public void grabDirectoryTree(String filename) {
		File cwd;
		boolean file=false;
		if(filename != null) {
			File f = new File(filename);
			if(!f.exists()) cwd = new File("./");
			else {
				cwd = new File(f.getPath());
				file=true;
			}
		}
		else cwd = new File("./");
		File[] dlist = cwd.listFiles(new ImageFileFilter());
		Arrays.sort(dlist);
		directory = new ImageFile[dlist.length];
		for(int i=0; i<dlist.length; i++) {
			directory[i] = new ImageFile(dlist[i]);
		}

		// Set the currentFile index
		if(file) {
			System.err.println("Loading a specific file is not yet implemented :(");
		}
		else currentFile=0;
	}

	public static String getFileExtension(File file) {
		String str = file.getName();
		int i = str.lastIndexOf(".");
		if(i == -1) return "";
		else {
			String ret = str.substring(i+1,str.length());
//			System.out.println("Returning extension: " + ret);
			return ret;
		}
	}
}


class ImageFileFilter implements FileFilter {
	public boolean accept(File file) {
		if(file.isDirectory() || !file.canRead()) return false;
		String ext = ImageViewer.getFileExtension(file);

		for(int i=0; i<ImageViewer.imageType.length; i++)
			if(ImageViewer.imageType[i].equalsIgnoreCase(ext)) return true;

		return false;
	}
}


/*
 * Helps ensure that heap space is not exceeded.
 * Silly idea though.  Not sure how wise this is.
 *
 * Makes sure that only two prev and two next images are kept in memory at a time.
 */
class CleanupThread extends Thread {
	public boolean alive=true;
	ImageViewer parent;
	ImageFile[] directory;
		
	
	public CleanupThread(ImageViewer p, ImageFile[] d) {
		parent = p;
		directory = d;
	}
	
	
	public void run() {
		int len = directory.length;
		while(alive) {
			for(int i=0; i<directory.length; i++) {
				try { sleep(100); }
				catch(Exception e) { e.printStackTrace(); }
				
				int idx = parent.currentFile;
				int distToEnd = len-i;
				
				// Continue if within valid range
				if(Math.abs(i-idx) < 2) continue;
				if(i < 2 && len-idx > 2) continue;
				if(distToEnd < 2 && idx < 2) continue;
					
				directory[i].unloadImage();
			}
		}
	}
}


/*
 * Thread to load the images, keeping the program itself smooth
 */

class LoadImageThread extends Thread {
	ImageViewer parent;
	int idx, extra;
	boolean setPanel;

	/*
	 * parent, the ImageViewer calling it.  Should always be 'this'
	* idx, the index of the file to read
	* extra, the distance next and prev of idx to load as well
	* setPanel, whether to set the panel in the parent to the image loaded
	 */
	public LoadImageThread(ImageViewer parent, int idx, int extra, boolean setPanel) {
		this.parent = parent;
		this.idx = idx;
		this.extra = extra;
		this.setPanel = setPanel;
	}
	
	public void loadImage(int idx) {
		Container c = parent.getContentPane();
		int winWidth=parent.DEFAULT_WIDTH, winHeight=parent.DEFAULT_HEIGHT;

		ImagePanel pan = parent.imagePanel;
		if(pan != null) {
			winWidth = pan.getWidth();
			winHeight = pan.getHeight();
		}
		pan = parent.directory[idx].getImagePanel();

		
		if(parent.scaleImage) {
			int picWidth = pan.getImageWidth();
			int picHeight = pan.getImageHeight();
			
			// determine optimal scale
			double ratioW = winWidth/(double)picWidth;
			double ratioH = winHeight/(double)picHeight;
			double rat = Math.min(ratioW,ratioH);
			if(rat > 0.00001f) pan.scale(rat);
				
			System.out.println(parent.directory[idx].getAbsolutePath() + "\n" + 
						"Image: " + picWidth + "x" + picHeight + "\n" +
						"Panel: " + winWidth + "x" + winHeight + "\n" +
						"Ratios: " + ratioW + ", " + ratioH + "\n");
		}
	}
	
	public void setParentPanel(int idx) {
		Container c = parent.getContentPane();
		
		if(parent.imagePanel != null) c.remove(parent.imagePanel);
		parent.imagePanel = parent.directory[idx].getImagePanel();
		parent.statusBar.setText(parent.directory[idx].getAbsolutePath());
		c.add(parent.imagePanel, BorderLayout.CENTER);
		
		parent.imagePanel.repaint();
		parent.validate();
	}
	
	public void run() {
		// Load the current image, the next image, and the previous image.
		int len = parent.directory.length;
		
		loadImage(idx);
		if(setPanel) setParentPanel(idx);

		if(extra > 0) {
			extra--;

			// Load next
			int next;
			if(idx==len-1) next=0;
			else next=idx+1;
			LoadImageThread n = new LoadImageThread(parent,next,extra,false);
			n.start();
		
			// Load prev
			int prev;
			if(idx==0) prev=len-1;
			else prev=idx-1;
			LoadImageThread p = new LoadImageThread(parent,prev,extra,false);
			p.start();
		}
	}
}


class PlaybackThread extends Thread {
	private int currentFrame;
	private int numFrames;
	private boolean alive=true;
	ImageViewer parent;

	
	public PlaybackThread(ImageViewer parent, int start, int end) {
		this.parent = parent;
		currentFrame = start;
		numFrames = end;
	}
	
	public void run() {
		while(currentFrame <= numFrames && alive) {
			parent.setImage(++currentFrame);
			try {
				sleep(10);
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		parent.movieButton.setText("Play |>");
		parent.movieButton.playing=false;
	}

	public void stopPlayback() { alive=false; }
}



/** The movie playback button.
 * This helps to keep all the code for
 * it contained in the same area.
 */
class MovieButton extends JButton implements ActionListener {
	public boolean playing=false;
	PlaybackThread moviePlaybackThread=null;
	ImageViewer parent;
	
	
	public void actionPerformed(ActionEvent e) {
		if(playing) {
			playing = false;
			if(moviePlaybackThread != null) 
				moviePlaybackThread.stopPlayback();
			setText("Play |>");
		}
	
		else {
			playing = true;
			if(moviePlaybackThread != null) {
				moviePlaybackThread.stopPlayback();
			}
			setText("Stop []");
			moviePlaybackThread = 
				new PlaybackThread(
					parent,
					parent.currentFile,
					parent.directory.length
				);
			moviePlaybackThread.start();
		}
	}
	
	
	public MovieButton(ImageViewer parent) {
		this.parent = parent;
		addActionListener(this);
		setText("Play |>");
	}
}


class ImageViewerMenuBar extends JMenuBar {
	ImageViewer parent;
	
	
	public ImageViewerMenuBar(ImageViewer parent) {
		this.parent = parent;
		
		
		// The "Directory" menu
		JMenu directory = new JMenu("Directory");
		add(new JScrollPane(directory));
		
		// Build all JMenuItems for the "Directory" menu
		for(int i=0; i<parent.directory.length; i++) {
			ImageIcon icon = new ImageIcon(parent.directory[i].toString());
			Image image = icon.getImage();
			directory.add(
				new JMenuItem(
					parent.directory[i].toString(),
					new ImageIcon(image.getScaledInstance(40,30, Image.SCALE_FAST))
				)
			);
		}
	}
}
