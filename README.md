# README #

This is an image viewer program that I wrote in Java in the early 2000's for use in Linux, as I did not have an adequate one at the time. There are now much better programs available.

Likely requires Java 1.4 or later.

To run, compile source and run the ImageViewer class.