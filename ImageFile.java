import java.io.File;
import javax.swing.*;


public class ImageFile {
	
	ImagePanel image;
	File filename;
	
	
	public ImageFile() {
		image = null;
		filename = null;
	}
	public ImageFile(File filename) {
		image = null;
		this.filename = filename;
	}
	public ImageFile(File filename, ImageIcon img) {
		image = new ImagePanel(img);
		this.filename = filename;
	}
	
	public void setFilename(File filename) {
		if(this.filename.equals(filename)) return;
		
		this.filename = filename;
		image = null;	// Changing the filename changes the file, too
	}
	
	
	
	public ImagePanel getImagePanel() { 
		if(image == null) loadImage();
		return image;
	}
	
	public void loadImage() {
		System.out.println("Loading image " + filename.getAbsolutePath() + "...");
		ImageIcon icon = new ImageIcon(filename.getAbsolutePath());
		image = new ImagePanel(icon);
	}
	
	public void unloadImage() {
		if(image != null) {
			System.out.println("Unloading " + filename.getAbsolutePath() + "...");
			image = null;
		}
	}
	
	public String getAbsolutePath() { return filename.getAbsolutePath(); }
}