import java.awt.*;
import javax.swing.*;
import java.util.HashMap;


public class ImagePanel extends JComponent {
	ImageIcon originalImage, currentImage;
	Dimension size;	// Size of the original image
	static int hints = Image.SCALE_FAST;
	
	public ImagePanel() {
		originalImage = currentImage = null;
		size = new Dimension(0,0);
	}
	
	public ImagePanel(ImageIcon image) {
		setImage(image);
	}
	
	
	public void setImage(ImageIcon img) {
		// Clear old images
		originalImage = currentImage = img;
		size = new Dimension( img.getIconWidth(), img.getIconHeight() );
	}
	
	public void paintComponent(Graphics g) {
		int panW = getWidth();
		int imgW = currentImage.getIconWidth();
		int panH = getHeight();
		int imgH = currentImage.getIconHeight();
		int offsetX = 0;
		int offsetY = 0;
		
		if(panW > imgW) offsetX = (panW - imgW)/2;
		if(panH > imgH) offsetY = (panH - imgH)/2;
			
		currentImage.paintIcon(this, g, offsetX, offsetY);
	}
	
	public void scale(double factor) {
		scale((int)(size.width*factor),(int)(size.height*factor));
	}
	
	public void scale(int w, int h) {
		if(originalImage == null) return;
		
		System.out.println("Resizing image to " + w + "x" + h + "...");
		currentImage = new ImageIcon(originalImage.getImage().getScaledInstance(w,h,hints));
	}
	
	
	public int getImageWidth() { return originalImage.getIconWidth(); }
	public int getImageHeight() { return originalImage.getIconHeight(); }
}

class Dimension {
	public int width, height;
	public Dimension(int w, int h) { 
		width = w;
		height = h;
	}
	
	public boolean equals(Dimension d) {
		if(d.width == width && d.height == height) return true;
		return false;
	}
	
	public String toString() { return width + "x" + height; }
}